#ADMIN STUFF

## Installation
This a project based on Yoeman KeystoneJS generator.  [Here is how to install KeystoneJS using Yo.](http://keystonejs.com/getting-started/)

## MongoDB
mango db needs to run on my local machine prior to starting this back end:

```
mongod --dbpath ~/Documents/mongo/data/db
```

## Cloudinary
- [https://cloudinary.com/console/](https://cloudinary.com/console/)
- This will help manage the pictures in everyway: storing, resizing.

## TedAir BE
admin account: boostup@gmail.com | admin

```
node keystone
```

Access the front end at [http://localhost:3000/](http://localhost:3000/)


# PROJECT DESCRIPTION

This project is meant to be the Back End(BE) for the tedair mobile app. 
Because it brings together tightly nodejs and mongodb, and cloudinary, KeystoneJS seems to be the prefect theoretical choice for the functional requirements of this project.

## BE features:
- Preprocessing to create a TED Video Catalog: 
	- Preprocessing of the TED RSS feed on a regular basis so as to create and maintain an independant datastore containing all the metadata of available TED videos
	- Videos will not be copied and stored.  The video URL will then still be the one found in the TED RSS feed.

- REST API
	- User management ??  Mainly so that the user of the mobile app can create a favorites list of videos.  LocalStorage might be sufficient.
	- TED Video Catalog:
		- get list of all videos with pagination for infinite scroll
		- get details for video by id

 
## TODOS

- Preprocessing:
	- set up a daemon with NodeJS
	- daemon script that will fetch the JSON from the RSS feed
	- script that will create the db objects based on the RSS data

- Cloudinary:
	- store in cloudinary the images referenced in the RSS data and cross reference cloudinary and database:
		- [http://cloudinary.com/documentation/node_integration#getting_started_guide](http://cloudinary.com/documentation/node_integration#getting_started_guide)
		- [http://cloudinary.com/documentation/fetch_remote_images#fetch_image](http://cloudinary.com/documentation/fetch_remote_images#fetch_image)
	- define responsive images breakpoints => [http://www.responsivebreakpoints.com/](http://www.responsivebreakpoints.com/)
	- have cloudinary create the images according to these breakpoints
